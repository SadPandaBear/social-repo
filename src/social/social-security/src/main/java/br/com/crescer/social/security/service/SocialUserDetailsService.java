package br.com.crescer.social.security.service;

import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.security.enumeration.SocialRoles;
import br.com.crescer.social.service.services.PerfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Carlos H. Nonnemacher
 */
@Service
public class SocialUserDetailsService implements UserDetailsService {
    
    private static final String CRESCER = "crescer";
    
    @Autowired
    PerfilService service = new PerfilService();
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.isEmpty()) {
            throw new UsernameNotFoundException(String.format("User with username=%s was not found", username));
        }
        Perfil p = service.findByDsEmail(username);
        return new UserModel(p.getIdPerfil(), p.getIdPessoa().getNmPessoa(), p.getDsEmail(), p.getDsSenha(), SocialRoles.valuesToList());
    }

}
