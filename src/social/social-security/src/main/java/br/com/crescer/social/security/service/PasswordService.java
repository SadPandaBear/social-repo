/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.security.service;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas.lentz
 */
@Service
public class PasswordService {    
    public String encodePassword(String rawPass) {
        return new BCryptPasswordEncoder().encode(rawPass);
    }
    
    public boolean confirmarSenha(String senha, String senhaConfirmada){
        return senha.equals(senhaConfirmada);
    }
}
