/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.service.services;

import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.entity.entities.Relacionamento;
import br.com.crescer.social.entity.entities.Solicitacao;
import br.com.crescer.social.service.repository.SolicitacaoRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas.lentz
 */
@Service
public class SolicitacaoService {
    final String APROVADO = "A";
    final String PENDENTE = "P";
    final String REJEITADO = "R";
    
    @Autowired
    SolicitacaoRepository repository;
    
    @Autowired
    RelacionamentoService serviceR;
    
    @Autowired
    PerfilService service;
    
    public void solicitarAmizade(Long id) {
        Perfil perfil = this.getLoggedUser();
        
        Solicitacao sol = new Solicitacao(PENDENTE, id, perfil);
        
        repository.save(sol);
    }
    
    public Iterable<Solicitacao> listAll() {
        return repository.listAllSolicitacao(getLoggedUser().getIdPerfil());
    }
    
    public void delete(Long id) {
        Perfil perfil = this.getLoggedUser();
        repository.deleteSol(PENDENTE, id, perfil.getIdPerfil());
    }
    
    public void accept(Long id) {
        Perfil perfil = this.getLoggedUser();
        
        repository.update(APROVADO, id, perfil.getIdPerfil());
        
        Relacionamento r = new Relacionamento();
        r.setDtRelacionamento(new Date());
        r.setIdPerfil(perfil);
        r.setIdPerfilRelacionamento(id);
        serviceR.save(r);
    }
    
    public boolean temSolicitacaoCom(Long id){
        Perfil perfil = this.getLoggedUser();
        return repository.getSolicitacoesCom(perfil.getIdPerfil(), id) > 0;
    }
    
    public int getCountedSolicitacao(){
        Perfil perfil = this.getLoggedUser();
        return repository.getSolicitacoesCom(perfil.getIdPerfil());
    }
    
    private Perfil getLoggedUser() {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return service.findByDsEmail(user.getUsername());
    }
    
    public Long getLoggedUserId() {
        return getLoggedUser().getIdPerfil();
    }
}
