/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.service.repository;

import br.com.crescer.social.entity.entities.Relacionamento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lucas Lentz
 */
public interface RelacionamentoRepository extends CrudRepository<Relacionamento, Long> {
    
}
