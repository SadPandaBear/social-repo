package br.com.crescer.social.service.repository;

import br.com.crescer.social.entity.entities.Perfil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PerfilRepository extends CrudRepository<Perfil, Long>{
    @Query(value = "SELECT P FROM Perfil P where P.dsEmail = %:#{#dsEmail}%")
    Perfil findOneByDsEmail(@Param("dsEmail") String dsEmail);
    
    @Query(value = "SELECT P FROM Perfil P WHERE P.idPessoa.nmPessoa Like %:nome% "
            + "AND ( :idPerfil "
            + "NOT IN (SELECT R.idPerfil.idPerfil FROM Relacionamento R WHERE R.idPerfil.idPerfil = P.idPerfil OR R.idPerfilRelacionamento = P.idPerfil) "
            + "AND :idPerfil "
            + "NOT IN (SELECT R.idPerfilRelacionamento FROM Relacionamento R WHERE R.idPerfilRelacionamento = P.idPerfil OR R.idPerfil.idPerfil = P.idPerfil))")
    Iterable<Perfil> findOneByNmPessoa(@Param("nome") String nome, @Param("idPerfil") Long idPerfil);
    
    @Query(value = "SELECT P FROM Perfil P WHERE P.idPessoa.nmPessoa Like %:nome% "
            + "AND ( :idPerfil "
            + "IN (SELECT R.idPerfil.idPerfil FROM Relacionamento R WHERE R.idPerfil.idPerfil = P.idPerfil OR R.idPerfilRelacionamento = P.idPerfil) "
            + "OR :idPerfil "
            + "IN (SELECT R.idPerfilRelacionamento FROM Relacionamento R WHERE R.idPerfilRelacionamento = P.idPerfil OR R.idPerfil.idPerfil = P.idPerfil))"
            + "AND P.idPerfil != :idPerfil")
    Iterable<Perfil> findAllFriends(@Param("nome") String nome, @Param("idPerfil") Long idPerfil);
}
