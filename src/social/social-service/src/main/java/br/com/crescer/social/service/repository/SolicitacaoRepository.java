/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.service.repository;

import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.entity.entities.Solicitacao;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SolicitacaoRepository extends CrudRepository<Solicitacao, Long>{
    @Query("SELECT COUNT(1) FROM Solicitacao S WHERE :perfil1 != :perfil2 AND "
            + ":perfil1 IN (S.idPerfil.idPerfil, S.idPerfilSolicitacao) AND "
            + ":perfil2 IN (S.idPerfil.idPerfil, S.idPerfilSolicitacao)")
    public int getSolicitacoesCom(@Param("perfil1") Long perfil1, @Param("perfil2") Long perfil2);
    
    @Query("SELECT COUNT(1) FROM Solicitacao S WHERE :perfil1 IN (S.idPerfilSolicitacao) AND S.tpStatusSolicitacao = 'P'")
    public int getSolicitacoesCom(@Param("perfil1") Long perfil1);
    
    @Query("SELECT S FROM Solicitacao S WHERE (:perfil1 IN (S.idPerfilSolicitacao) AND S.tpStatusSolicitacao = 'P') AND S.tpStatusSolicitacao = 'P')")
    public Iterable<Solicitacao> listAllSolicitacao(@Param("perfil1") Long perfil1);

    @Modifying
    @Transactional
    @Query("DELETE FROM Solicitacao S WHERE S.idPerfil.idPerfil = :idDominante AND S.idPerfilSolicitacao = :idDominado AND S.tpStatusSolicitacao = :msg")
    public void deleteSol(@Param("msg") String msg,@Param("idDominante") Long id1, @Param("idDominado") Long id2);
    
    @Modifying
    @Transactional
    @Query("UPDATE Solicitacao S SET S.tpStatusSolicitacao = :msg WHERE S.idPerfil.idPerfil = :idDominante AND S.idPerfilSolicitacao = :idDominado AND :idDominado != :idDominante")
    public void update(@Param("msg") String msg, @Param("idDominante") Long idDominante, @Param("idDominado") Long idDominado);

}
