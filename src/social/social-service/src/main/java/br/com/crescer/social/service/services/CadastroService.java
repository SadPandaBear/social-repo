package br.com.crescer.social.service.services;

import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.entity.entities.Relacionamento;
import br.com.crescer.social.service.repository.CadastroRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CadastroService {
    @Autowired
    CadastroRepository repository;
    
    @Autowired
    RelacionamentoService service;
    
    public void save(Perfil p) {
        repository.save(p);
        Relacionamento rel = new Relacionamento();
        rel.setIdPerfil(p);
        rel.setIdPerfilRelacionamento(p.getIdPerfil());
        rel.setDtRelacionamento(new Date());
        service.save(rel);
    }
}
