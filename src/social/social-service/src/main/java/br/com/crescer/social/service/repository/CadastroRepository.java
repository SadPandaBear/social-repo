package br.com.crescer.social.service.repository;

import br.com.crescer.social.entity.entities.Perfil;
import org.springframework.data.repository.CrudRepository;

public interface CadastroRepository extends CrudRepository<Perfil, Long> {
}
