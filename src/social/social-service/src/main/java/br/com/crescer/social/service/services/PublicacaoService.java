package br.com.crescer.social.service.services;

import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.entity.entities.Publicacao;
import br.com.crescer.social.service.repository.PublicacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
public class PublicacaoService {
    
    @Autowired
    PublicacaoRepository repository;
    
    @Autowired
    PerfilService service;
    
    public Publicacao save(Publicacao p) {
        return repository.save(p);
    }
    
    public Iterable<Publicacao> listAll(Pageable pageable) {
        pageable = new PageRequest(pageable.getPageNumber(), 8, Sort.Direction.DESC, "dtPublicacao");
        return repository.findAll(pageable);
    }
    
    public Iterable<Publicacao> findByIdPerfil(Pageable pageable) { 
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        pageable = new PageRequest(pageable.getPageNumber(), 8, Sort.Direction.DESC, "dtPublicacao");
        Perfil perfil = service.findByDsEmail(user.getUsername());
        return repository.findByIdPerfil(pageable, perfil.getIdPerfil());
    }
}
