package br.com.crescer.social.service.services;

import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.service.repository.PerfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
public class PerfilService {
    
    @Autowired
    PerfilRepository repository;
    
    public Perfil findByDsEmail(String dsEmail) {        
        return repository.findOneByDsEmail(dsEmail);
    }
    
    public Perfil findById(Long id) {        
        return repository.findOne(id);
    }
    
    public Iterable<Perfil> findByNmPessoa(String nome) {    
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Perfil perfil = findByDsEmail(user.getUsername());
        return repository.findOneByNmPessoa(nome, perfil.getIdPerfil());
    }
    
    public Iterable<Perfil> listAllFriends(String nome) {    
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Perfil perfil = findByDsEmail(user.getUsername());
        return repository.findAllFriends(nome, perfil.getIdPerfil());
    }
}
