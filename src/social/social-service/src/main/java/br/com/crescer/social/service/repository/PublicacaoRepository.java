/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.service.repository;

import br.com.crescer.social.entity.entities.Publicacao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Lucas Lentz
 */
public interface PublicacaoRepository extends PagingAndSortingRepository<Publicacao, Long>{
    @Query("SELECT P FROM Publicacao P "
            + "WHERE :idPerfil = :idPerfil AND :idPerfil IN(SELECT R.idPerfil.idPerfil FROM Relacionamento R WHERE R.idPerfil.idPerfil = P.idPerfil.idPerfil OR R.idPerfilRelacionamento = P.idPerfil.idPerfil)"
            + "OR :idPerfil IN(SELECT R.idPerfilRelacionamento FROM Relacionamento R WHERE R.idPerfil.idPerfil = P.idPerfil.idPerfil OR R.idPerfilRelacionamento = P.idPerfil.idPerfil)")
    Page<Publicacao> findByIdPerfil(Pageable pageable, @Param("idPerfil") Long idPerfil);
}
