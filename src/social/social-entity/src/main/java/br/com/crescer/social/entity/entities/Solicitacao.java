/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.entity.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.lentz
 */
@Entity
@Table(name = "SOLICITACAO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Solicitacao.findAll", query = "SELECT s FROM Solicitacao s"),
    @NamedQuery(name = "Solicitacao.findByIdSolicitacao", query = "SELECT s FROM Solicitacao s WHERE s.idSolicitacao = :idSolicitacao"),
    @NamedQuery(name = "Solicitacao.findByTpStatusSolicitacao", query = "SELECT s FROM Solicitacao s WHERE s.tpStatusSolicitacao = :tpStatusSolicitacao"),
    @NamedQuery(name = "Solicitacao.findByIdPerfilSolicitacao", query = "SELECT s FROM Solicitacao s WHERE s.idPerfilSolicitacao = :idPerfilSolicitacao")})
public class Solicitacao implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITACAO")
    @SequenceGenerator(name = "SEQ_SOLICITACAO", sequenceName = "SEQ_SOLICITACAO", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "ID_SOLICITACAO")
    private Long idSolicitacao;
    @Basic(optional = false)
    @Column(name = "TP_STATUS_SOLICITACAO")
    private String tpStatusSolicitacao;
    @Basic(optional = false)
    @Column(name = "ID_PERFIL_SOLICITACAO")
    private Long idPerfilSolicitacao;
    @JoinColumn(name = "ID_PERFIL", referencedColumnName = "ID_PERFIL")
    @ManyToOne(optional = false)
    private Perfil idPerfil;

    public Solicitacao() {
    }

    public Solicitacao(Long idSolicitacao) {
        this.idSolicitacao = idSolicitacao;
    }

    public Solicitacao(String tpStatusSolicitacao, Long idPerfilSolicitacao, Perfil idPerfil) {
        this.tpStatusSolicitacao = tpStatusSolicitacao;
        this.idPerfilSolicitacao = idPerfilSolicitacao;
        this.idPerfil = idPerfil;
    }
    
    public Solicitacao(Long idSolicitacao, String tpStatusSolicitacao, Long idPerfilSolicitacao) {
        this.idSolicitacao = idSolicitacao;
        this.tpStatusSolicitacao = tpStatusSolicitacao;
        this.idPerfilSolicitacao = idPerfilSolicitacao;
    }

    public Long getIdSolicitacao() {
        return idSolicitacao;
    }

    public void setIdSolicitacao(Long idSolicitacao) {
        this.idSolicitacao = idSolicitacao;
    }

    public String getTpStatusSolicitacao() {
        return tpStatusSolicitacao;
    }

    public void setTpStatusSolicitacao(String tpStatusSolicitacao) {
        this.tpStatusSolicitacao = tpStatusSolicitacao;
    }

    public Long getIdPerfilSolicitacao() {
        return idPerfilSolicitacao;
    }

    public void setIdPerfilSolicitacao(Long idPerfilSolicitacao) {
        this.idPerfilSolicitacao = idPerfilSolicitacao;
    }

    public Perfil getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Perfil idPerfil) {
        this.idPerfil = idPerfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSolicitacao != null ? idSolicitacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Solicitacao)) {
            return false;
        }
        Solicitacao other = (Solicitacao) object;
        if ((this.idSolicitacao == null && other.idSolicitacao != null) || (this.idSolicitacao != null && !this.idSolicitacao.equals(other.idSolicitacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.crescer.social.entity.entities.Solicitacao[ idSolicitacao=" + idSolicitacao + " ]";
    }
    
}
