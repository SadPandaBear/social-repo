/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.entity.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.lentz
 */
@Entity
@Table(name = "PUBLICACAOLIKE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Publicacaolike.findAll", query = "SELECT p FROM Publicacaolike p"),
    @NamedQuery(name = "Publicacaolike.findByIdPublicacaoLike", query = "SELECT p FROM Publicacaolike p WHERE p.idPublicacaoLike = :idPublicacaoLike"),
    @NamedQuery(name = "Publicacaolike.findByIdPerfil", query = "SELECT p FROM Publicacaolike p WHERE p.idPerfil = :idPerfil")})
public class Publicacaolike implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PUBLICACAO")
    @SequenceGenerator(name = "SEQ_PUBLICACAO", sequenceName = "SEQ_PUBLICACAO", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "ID_PUBLICACAO_LIKE")
    private Long idPublicacaoLike;
    @Basic(optional = false)
    @Column(name = "ID_PERFIL")
    private BigInteger idPerfil;
    @JoinColumn(name = "ID_PUBLICACAO", referencedColumnName = "ID_PUBLICACAO")
    @ManyToOne(optional = false)
    private Publicacao idPublicacao;

    public Publicacaolike() {
    }

    public Publicacaolike(Long idPublicacaoLike) {
        this.idPublicacaoLike = idPublicacaoLike;
    }

    public Publicacaolike(Long idPublicacaoLike, BigInteger idPerfil) {
        this.idPublicacaoLike = idPublicacaoLike;
        this.idPerfil = idPerfil;
    }

    public Long getIdPublicacaoLike() {
        return idPublicacaoLike;
    }

    public void setIdPublicacaoLike(Long idPublicacaoLike) {
        this.idPublicacaoLike = idPublicacaoLike;
    }

    public BigInteger getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(BigInteger idPerfil) {
        this.idPerfil = idPerfil;
    }

    public Publicacao getIdPublicacao() {
        return idPublicacao;
    }

    public void setIdPublicacao(Publicacao idPublicacao) {
        this.idPublicacao = idPublicacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPublicacaoLike != null ? idPublicacaoLike.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publicacaolike)) {
            return false;
        }
        Publicacaolike other = (Publicacaolike) object;
        if ((this.idPublicacaoLike == null && other.idPublicacaoLike != null) || (this.idPublicacaoLike != null && !this.idPublicacaoLike.equals(other.idPublicacaoLike))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.crescer.social.entity.entities.Publicacaolike[ idPublicacaoLike=" + idPublicacaoLike + " ]";
    }
    
}
