/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.entity.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.lentz
 */
@Entity
@Table(name = "PESSOA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pessoa.findAll", query = "SELECT p FROM Pessoa p"),
    @NamedQuery(name = "Pessoa.findByIdPessoa", query = "SELECT p FROM Pessoa p WHERE p.idPessoa = :idPessoa"),
    @NamedQuery(name = "Pessoa.findByNmPessoa", query = "SELECT p FROM Pessoa p WHERE p.nmPessoa = :nmPessoa"),
    @NamedQuery(name = "Pessoa.findByDtNascPessoa", query = "SELECT p FROM Pessoa p WHERE p.dtNascPessoa = :dtNascPessoa"),
    @NamedQuery(name = "Pessoa.findByTpSexoPessoa", query = "SELECT p FROM Pessoa p WHERE p.tpSexoPessoa = :tpSexoPessoa")})
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PESSOA")
    @SequenceGenerator(name = "SEQ_PESSOA", sequenceName = "SEQ_PESSOA", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "ID_PESSOA")
    private Long idPessoa;
    @Basic(optional = false)
    @Column(name = "NM_PESSOA")
    private String nmPessoa;
    @Basic(optional = false)
    @Column(name = "DT_NASC_PESSOA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtNascPessoa;
    @Basic(optional = false)
    @Column(name = "TP_SEXO_PESSOA")
    private String tpSexoPessoa;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "idPessoa")
    private Perfil perfil;

    public Pessoa() {
    }

    public Pessoa(Long idPessoa) {
        this.idPessoa = idPessoa;
    }

    public Pessoa(Long idPessoa, String nmPessoa, Date dtNascPessoa, String tpSexoPessoa) {
        this.idPessoa = idPessoa;
        this.nmPessoa = nmPessoa;
        this.dtNascPessoa = dtNascPessoa;
        this.tpSexoPessoa = tpSexoPessoa;
    }

    public Long getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Long idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNmPessoa() {
        return nmPessoa;
    }

    public void setNmPessoa(String nmPessoa) {
        this.nmPessoa = nmPessoa;
    }

    public Date getDtNascPessoa() {
        return dtNascPessoa;
    }

    public void setDtNascPessoa(Date dtNascPessoa) {
        this.dtNascPessoa = dtNascPessoa;
    }

    public String getTpSexoPessoa() {
        return tpSexoPessoa;
    }

    public void setTpSexoPessoa(String tpSexoPessoa) {
        this.tpSexoPessoa = tpSexoPessoa;
    }

    @JsonIgnore
    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPessoa != null ? idPessoa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pessoa)) {
            return false;
        }
        Pessoa other = (Pessoa) object;
        if ((this.idPessoa == null && other.idPessoa != null) || (this.idPessoa != null && !this.idPessoa.equals(other.idPessoa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.crescer.social.entity.entities.Pessoa[ idPessoa=" + idPessoa + " ]";
    }
    
}
