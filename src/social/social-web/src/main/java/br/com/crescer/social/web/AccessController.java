package br.com.crescer.social.web;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Carlos H. Nonnemacher
 */
@Controller
public class AccessController {

    @RequestMapping(value="/login")
    String login() {
        return "login";
    } 
    
    @RequestMapping(value="/logout")
    String logout(HttpSession httpSession) {
        httpSession.invalidate();
        return "redirect:login";
    } 
    
}
