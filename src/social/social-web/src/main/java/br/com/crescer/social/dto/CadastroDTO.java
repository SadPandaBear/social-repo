package br.com.crescer.social.dto;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class CadastroDTO {

    @NotNull 
    private String email;
    @NotNull
    private String senha;
    @NotNull
    private String confirmarSenha;
    @NotNull
    private String nome;
    @NotNull
    private Date dataNascimento;
    @NotNull
    private String sexo;
    
    public CadastroDTO(){
    }
    
    public CadastroDTO(String email, String senha, String confirmarSenha, String nome, Date dataNascimento, String sexo) {
        this.email = email;
        this.senha = senha;
        this.confirmarSenha = confirmarSenha;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public String getSenha() {
        return senha;
    }

    public String getNome() {
        return nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public String getConfirmarSenha() {
        return confirmarSenha;
    }
    
    public String getSexo() {
        return sexo;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void setConfirmarSenha(String confirmarSenha) {
        this.confirmarSenha = confirmarSenha;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
