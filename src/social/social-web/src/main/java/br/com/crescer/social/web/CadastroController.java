package br.com.crescer.social.web;

import br.com.crescer.social.dto.CadastroDTO;
import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.entity.entities.Pessoa;
import br.com.crescer.social.security.service.PasswordService;
import br.com.crescer.social.service.services.CadastroService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CadastroController {

    @Autowired
    CadastroService service;

    @Autowired
    PasswordService pService;
    
    @RequestMapping(value="/cadastro")
    String cadastro(Model model) {
        model.addAttribute("perfil", new CadastroDTO());
        return "cadastro";
    }
    
    @RequestMapping(value="/cadastro/salvar", method = RequestMethod.POST)
    String salvar(@Valid CadastroDTO dto, BindingResult result){  
        if(result.hasErrors()) {
            return "redirect:/cadastro";
        }
        Perfil p = new Perfil();
        Pessoa pe = new Pessoa();
        
        pe.setNmPessoa(dto.getNome());
        pe.setTpSexoPessoa(dto.getSexo());
        pe.setDtNascPessoa(dto.getDataNascimento());
        p.setDsEmail(dto.getEmail());
        p.setDsSenha(pService.encodePassword(dto.getSenha()));
        p.setIdPessoa(pe);
        
        service.save(p);
        
        return "login";
    }
}
