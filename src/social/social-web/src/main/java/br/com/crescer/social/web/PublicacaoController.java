package br.com.crescer.social.web;

import br.com.crescer.social.dto.PublicacaoDTO;
import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.entity.entities.Publicacao;
import br.com.crescer.social.service.services.PerfilService;
import br.com.crescer.social.service.services.PublicacaoService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/publicacao")
public class PublicacaoController {
    
    @Autowired
    PublicacaoService service; 
    
    @Autowired
    PerfilService servicePs;
    
    @ResponseBody
    @RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
    public Iterable<Publicacao> list(Pageable pageable) {
        // Iterable<Publicacao> p = service.listAll(pageable);
        return service.findByIdPerfil(pageable);
    }
    
    @RequestMapping(value="/save", method = RequestMethod.POST)
    public String save(Model model, @Valid PublicacaoDTO dto, BindingResult result) {
        if (result.hasErrors()) {
            return "redirect:/home";
        }
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Perfil perfil = servicePs.findByDsEmail(user.getUsername());
                
        Publicacao pu = new Publicacao();
        pu.setTpPublicacao(dto.getTpPublicacao());
        pu.setIdPerfil(perfil);
        pu.setDtPublicacao(dto.getDtPublicacao());
        
        service.save(pu);
        
        model.addAttribute("publicacaoDTO", new PublicacaoDTO());
        return "redirect:/home";
    }
}
