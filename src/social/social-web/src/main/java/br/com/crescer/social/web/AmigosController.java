/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.web;

import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.service.services.PerfilService;
import br.com.crescer.social.service.services.SolicitacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AmigosController {
    
    @Autowired
    PerfilService servicePs;
    
    @Autowired
    SolicitacaoService service;
    
    @RequestMapping(value="/amigos")
    String cadastro(Model model, Long id) {
        model.addAttribute("idPerfil", id);
        return "amigos";
    }
    
    @RequestMapping(value="/profile")
    String profile(Model model, Long id) {
        Perfil perfil = servicePs.findById(id);
        model.addAttribute("perfil", perfil);
        model.addAttribute("perfSol", !id.equals(service.getLoggedUserId()));
        model.addAttribute("sol", service.temSolicitacaoCom(id));
        return "profile";
    }
    
    @RequestMapping(value="/profile/me")
    String profileMe(Model model) {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Perfil perfil = servicePs.findByDsEmail(user.getUsername());
        return profile(model, perfil.getIdPerfil() );
    }
}
