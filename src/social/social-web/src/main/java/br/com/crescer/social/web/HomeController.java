package br.com.crescer.social.web;

import br.com.crescer.social.dto.PublicacaoDTO;
import br.com.crescer.social.entity.entities.Perfil;
import br.com.crescer.social.service.services.PerfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @Autowired
    PerfilService servicePs;
    
    @RequestMapping(value="/home")
    String home(Model model) {
        model.addAttribute("publicacaoDTO", new PublicacaoDTO());
        return "home";
    }
    
    @ResponseBody
    @RequestMapping(value="/unfriends/list", method = RequestMethod.GET, produces = "application/json")
    public Iterable<Perfil> listByIdPerfil(String nome) {
        // Iterable<Publicacao> p = service.listAll(pageable);
        return servicePs.findByNmPessoa(nome);
    }
    
    @ResponseBody
    @RequestMapping(value="/friends/list", method = RequestMethod.GET, produces = "application/json")
    public Iterable<Perfil> listAllFriends(String nome) {
        // Iterable<Publicacao> p = service.listAll(pageable);
        return servicePs.listAllFriends(nome);
    }
}
