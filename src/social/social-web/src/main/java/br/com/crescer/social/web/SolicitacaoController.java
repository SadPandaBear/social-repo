/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.web;

import br.com.crescer.social.entity.entities.Solicitacao;
import br.com.crescer.social.service.services.SolicitacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author lucas.lentz
 */
@Controller
public class SolicitacaoController {
    @Autowired
    SolicitacaoService service;
    
    @ResponseBody
    @RequestMapping(value="/rest/solicitar", method = RequestMethod.POST, produces = "application/json")
    public void solicitarAmizade(Long id) {
        service.solicitarAmizade(id);
    }
    
    @RequestMapping(value="/notifications", method = RequestMethod.GET)
    public String notificacoesHistory(Model model) {
        Iterable<Solicitacao> sol = service.listAll();
        model.addAttribute("solicitacoes", sol);
        return "notifications";
    }
    
    @ResponseBody
    @RequestMapping(value="/rest/solicitar/quant", method = RequestMethod.GET, produces = "application/json")
    public Integer getQuantNotification() {
        return new Integer(service.getCountedSolicitacao());
    }
    
    @RequestMapping(value="/solicitar/recuse")
    public String recuseSolitacao(Long id) {
        service.delete(id);
        return "redirect:/notifications";
    }
    
    @RequestMapping(value="/solicitar/accept")
    public String acceptSolitacao(Long id) {
        service.accept(id);
        return "redirect:/notifications";
    }
}
