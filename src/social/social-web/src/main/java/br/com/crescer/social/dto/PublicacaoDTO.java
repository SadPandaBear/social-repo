package br.com.crescer.social.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;

public class PublicacaoDTO {
    
    @NotNull
    private String tpPublicacao;
    
    public PublicacaoDTO(){
    }
    
    public PublicacaoDTO(String tpPublicacao){
        this.tpPublicacao = tpPublicacao;
    }
    
    public String getTpPublicacao() {
        return tpPublicacao;
    }

    public void setTpPublicacao(String tpPublicacao) {
        this.tpPublicacao = tpPublicacao;
    }
    
    public Date getDtPublicacao() {
        return new Date();
    }
}
