function sendSolicitacao(idSol){
    return $.ajax({
        url: '/rest/solicitar',
        type: 'POST',
        data: {
            id: idSol
        }
    });
}

$("#btnSolicitacao").click(function(e) {
    e.preventDefault();
    $(this).removeClass("btn-info").addClass("btn-success").addClass("disabled");
});
