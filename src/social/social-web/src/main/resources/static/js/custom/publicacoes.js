'use strict';

var count = 0;

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() === $(document).height()) {
        count++;
        loadPg(count);
    } else if ($(window).scrollTop() === 0){
        $('#publicacoes').empty();
        count = 0;
        loadPg();
    }
 });
     
function obter(count){
    return $.ajax({
        url: 'publicacao/list?page=' + count,
        type: 'GET'
    });
}

function obterJSON(count){
    var posts = $.ajax({
        url: 'publicacao/list?page=' + count,
        type: 'GET',
        async: false,
        dataType: 'json',
        done: function (jsonData) {
            JSON.parse(jsonData);
            return jsonData;
        }
    }).responseJSON;
    
    return posts;
}

function loadPg(count) {
    obter(count)
        .then(
            function OnSuccess(res) {
                debugger;
                var $publicacao = $('#publicacoes');
                res.content.forEach(function (cid) {
                    //debugger;
                    $publicacao.append(
                            '<div id="'+ cid.idPublicacao +'"class="container">'
                                +'<div class="panel panel-default">'
                                    +'<div class="panel-heading">'
                                        + cid.dtPublicacao + ' - ' + cid.idPerfil.idPessoa.nmPessoa
                                    +'</div>'
                                    +'<div class="panel-body">'
                                        + cid.tpPublicacao
                                        +'<br><button class="btn btn-success">'
                                            +'<i class="fa fa-thumbs-up"></i> '
                                            + cid.publicacaolikeCollection.length
                                        +'</button>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                            /*+'<span>' + cid.dtPublicacao + '</span>'
                            +'<span>' + cid.tpPublicacao + '</span>'
                            +'<span>' + cid.idPerfil.idPessoa.nmPessoa + '</span>'
                            +'<span>' + cid.publicacaolikeCollection.length + '</span>'*/
                            );
                    console.log(cid.nome);
                });
            },
            function OnError() {
                console.log("Ops!");
            }
        );
}
//loadPg();


// ANGULAR LEARNING

(function(){
    var app = angular.module('home', []);
     
    app.controller('PublicacaoController', ['$scope', function($scope){   
        $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
        };
            
        $scope.get = function (){ 
            return obterJSON();
        };
        
        this.publicacoes = $scope.get().content;
    }]);
})();


