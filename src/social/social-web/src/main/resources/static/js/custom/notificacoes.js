'use strict';

function getNotification(){
    return $.ajax({
        url: '/rest/solicitar/quant',
        type: 'GET'
    });
}

function setQuant() {
    getNotification()
            .then(
                function OnSuccess(res) {
                    var $notificacao = $('#notificacao');
                    if(res !== 0) $notificacao.text(res);
                },
                function OnError(){
                    console.log('Ops!');
                }
            );
}
setQuant();
setInterval((setQuant), 10000);
