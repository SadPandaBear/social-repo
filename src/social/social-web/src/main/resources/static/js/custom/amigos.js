'use strict';
     
function obter(query){
    var query = query || 'friends/list?nome=';
    return $.ajax({
        url: query,
        type: 'GET'
    });
}
function loadPg(query) {
    //debugger;
    obter(query)
        .then(
            function OnSuccess(res) {
                var $amigos = $('#amigos');
                //debugger;
                $amigos.empty();
                res.forEach(function (cid) {
                    //debugger;
                    $amigos.append(
                            '<div id="'+ cid.idPerfil +'"class="container col-xs-4">'
                                +'<div class="panel panel-default">'
                                    +'<div class="panel-heading">'
                                       + '<a class="btn btn-primary" href="profile?id=' + cid.idPerfil +'">' + cid.idPessoa.nmPessoa
                                       + '</a>'
                                    +'</div>'
                                    +'<div class="panel-body">'
                                        + 'Sexo: ' + cid.idPessoa.tpSexoPessoa + '<br>Nascimento: ' + cid.idPessoa.dtNascPessoa
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                            /*+'<span>' + cid.dtPublicacao + '</span>'
                            +'<span>' + cid.tpPublicacao + '</span>'
                            +'<span>' + cid.idPerfil.idPessoa.nmPessoa + '</span>'
                            +'<span>' + cid.publicacaolikeCollection.length + '</span>'*/
                            );
                    console.log(cid.idPerfil);
                });
            },
            function OnError() {
                console.log("Ops!");
            }
        );
}
loadPg();

$('#frmPessoa').submit(function (e) {
    loadPg('unfriends/list?nome=' + $('#nmPessoa').val());
    e.preventDefault();
});