CREATE TABLE Perfil
  (
    id_perfil INTEGER NOT NULL ,
    ds_email  VARCHAR2 (200) NOT NULL ,
    ds_senha  VARCHAR2 (200) NOT NULL ,
    id_pessoa INTEGER NOT NULL
  ) ;
CREATE UNIQUE INDEX Perfil__IDX ON Perfil
  (
    id_pessoa ASC
  )
  ;
ALTER TABLE Perfil ADD CONSTRAINT Perfil_PK PRIMARY KEY ( id_perfil ) ;
?
?
CREATE TABLE Pessoa
  (
    id_pessoa      INTEGER NOT NULL ,
    nm_pessoa      VARCHAR2 (255) NOT NULL ,
    dt_nasc_pessoa DATE NOT NULL ,
    tp_sexo_pessoa VARCHAR2 (10) NOT NULL
  ) ;
ALTER TABLE Pessoa ADD CONSTRAINT Pessoa_PK PRIMARY KEY ( id_pessoa ) ;
?
?
CREATE TABLE Publicacao
  (
    id_publicacao INTEGER NOT NULL ,
    dt_publicacao DATE NOT NULL ,
    tp_publicacao VARCHAR2 (4000) NOT NULL ,
    id_perfil     INTEGER NOT NULL
  ) ;
ALTER TABLE Publicacao ADD CONSTRAINT Publicacao_PK PRIMARY KEY ( id_publicacao ) ;
?
?
CREATE TABLE PublicacaoConteudo
  (
    id_publicacao_conteudo INTEGER NOT NULL ,
    conteudo  VARCHAR2
    --  ERROR: VARCHAR2 size not specified
    NOT NULL ,
    id_publicacao INTEGER NOT NULL
  ) ;
CREATE UNIQUE INDEX PublicacaoConteudo__IDX ON PublicacaoConteudo
  (
    id_publicacao ASC
  )
  ;
ALTER TABLE PublicacaoConteudo ADD CONSTRAINT PublicacaoConteudo_PK PRIMARY KEY ( id_publicacao_conteudo ) ;
?
?
CREATE TABLE PublicacaoLike
  (
    id_publicacao_like INTEGER NOT NULL ,
    id_publicacao      INTEGER NOT NULL ,
    id_perfil          INTEGER NOT NULL
  ) ;
ALTER TABLE PublicacaoLike ADD CONSTRAINT PublicacaoLike_PK PRIMARY KEY ( id_publicacao_like ) ;
?
?
CREATE TABLE Relacionamento
  (
    id_relacionamento        INTEGER NOT NULL ,
    dt_relacionamento        DATE NOT NULL ,
    id_perfil                INTEGER NOT NULL ,
    id_perfil_relacionamento INTEGER NOT NULL
  ) ;
ALTER TABLE Relacionamento ADD CONSTRAINT Relacionamento_PK PRIMARY KEY ( id_relacionamento ) ;
?
?
CREATE TABLE Solicitacao
  (
    id_solicitacao        INTEGER NOT NULL ,
    tp_status_solicitacao VARCHAR2 (10) NOT NULL ,
    id_perfil             INTEGER NOT NULL ,
    id_perfil_solicitacao INTEGER NOT NULL
  ) ;
ALTER TABLE Solicitacao ADD CONSTRAINT Solicitacao_PK PRIMARY KEY ( id_solicitacao ) ;
?
?
ALTER TABLE Perfil ADD CONSTRAINT Perfil_Pessoa_FK FOREIGN KEY ( id_pessoa ) REFERENCES Pessoa ( id_pessoa ) ;
?
--  ERROR: FK name length exceeds maximum allowed length(30)
ALTER TABLE PublicacaoConteudo ADD CONSTRAINT PublicacaoConteudo_Publi_FK FOREIGN KEY ( id_publicacao ) REFERENCES Publicacao ( id_publicacao ) ;
?
ALTER TABLE PublicacaoLike ADD CONSTRAINT PublicacaoLike_Publicacao_FK FOREIGN KEY ( id_publicacao ) REFERENCES Publicacao ( id_publicacao ) ;
?
ALTER TABLE Publicacao ADD CONSTRAINT Publicacao_Perfil_FK FOREIGN KEY ( id_perfil ) REFERENCES Perfil ( id_perfil ) ;
?
ALTER TABLE Relacionamento ADD CONSTRAINT Relacionamento_Perfil_FK FOREIGN KEY ( id_perfil ) REFERENCES Perfil ( id_perfil ) ;
?
ALTER TABLE Solicitacao ADD CONSTRAINT Solicitacao_Perfil_FK FOREIGN KEY ( id_perfil ) REFERENCES Perfil ( id_perfil ) ;